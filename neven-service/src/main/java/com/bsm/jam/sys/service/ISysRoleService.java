package com.bsm.jam.sys.service;

import com.bsm.jam.base.service.IBaseService;
import com.bsm.jam.sys.entity.SysRole;

import java.util.List;

/**
 * Created by bai on 2018/4/16.
 */
public interface ISysRoleService extends IBaseService<SysRole> {



    /**
     * 获取用户的角色和菜单
     *
     * @param userId
     * @return
     */
    List<SysRole> selectRoleByUserId(Long userId);
}
