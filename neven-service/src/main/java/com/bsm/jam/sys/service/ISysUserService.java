package com.bsm.jam.sys.service;

import com.bsm.jam.base.service.IBaseService;
import com.bsm.jam.sys.entity.SysUser;

/**
 * 用户管理接口
 * @author baijun
 * @data 2017年12月27日17:58:13
 */
public interface ISysUserService extends IBaseService<SysUser> {

    /**
     * 登录
     *
     * @param username
     * @return
     */
    SysUser getByLoginName(String username);
}
