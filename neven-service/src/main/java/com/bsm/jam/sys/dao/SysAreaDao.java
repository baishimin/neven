package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysArea;

/**
 * Created by chen on 2017/4/10.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe:  区域dao
 */
public interface SysAreaDao  extends CrudDao<SysArea> {


}
