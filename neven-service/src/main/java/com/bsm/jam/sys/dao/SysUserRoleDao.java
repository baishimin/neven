package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysUserRole;
import org.springframework.stereotype.Component;

/**
 * Created by chen on 2017/9/1.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 系统用户角色
 */
@Component
public interface SysUserRoleDao extends CrudDao<SysUserRole> {
}
