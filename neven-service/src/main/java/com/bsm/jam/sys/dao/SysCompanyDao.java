package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysCompany;

/**
 * Created by chen on 2017/4/10.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe:  公司信息 dao
 */
public interface SysCompanyDao extends CrudDao<SysCompany> {


}
