package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.TreeDao;
import com.bsm.jam.sys.entity.SysOffice;

/**
 * Created by chen on 2017/3/13.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 组织架构dao
 */
public interface SysOfficeDao extends TreeDao<SysOffice> {
}
