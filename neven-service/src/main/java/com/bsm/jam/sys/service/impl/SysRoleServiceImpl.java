package com.bsm.jam.sys.service.impl;

import com.bsm.jam.base.service.impl.BaseServiceImpl;
import com.bsm.jam.sys.dao.SysRoleDao;
import com.bsm.jam.sys.entity.SysRole;
import com.bsm.jam.sys.service.ISysRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bai on 2018/4/16.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleDao, SysRole> implements ISysRoleService {

    /**
     * 获取用户的角色和菜单
     *
     * @param userId
     * @return
     */
    @Override
    public List<SysRole> selectRoleByUserId(Long userId) {
        List<SysRole> sysRoleList=this.baseMapper.selectRoleByUserId(userId);
        return sysRoleList;
    }
}
