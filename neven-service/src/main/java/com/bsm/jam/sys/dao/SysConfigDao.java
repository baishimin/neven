package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysConfig;

/**
 * Created by chen on 2017/4/10.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 系统配置dao
 */
public interface SysConfigDao extends CrudDao<SysConfig> {
}
