package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysUserOffice;

/**
 *
 * @author chen
 * @date 2017/11/16
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe:
 */
public interface SysUserOfficeDao extends CrudDao<SysUserOffice> {
}
