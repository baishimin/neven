package com.bsm.jam.sys.service.impl;

import com.bsm.jam.base.service.impl.BaseServiceImpl;
import com.bsm.jam.sys.dao.SysUserDao;
import com.bsm.jam.sys.entity.SysUser;
import com.bsm.jam.sys.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * 用户管理实现类
 * @author baijun
 * @data 2017年12月27日18:01:18
 */
@Service
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class SysUserServiceImpl extends BaseServiceImpl<SysUserDao,SysUser> implements ISysUserService {

    @Override
    public SysUser getByLoginName(String username) {
        return this.baseMapper.selectByLoginName(username);
    }
}
