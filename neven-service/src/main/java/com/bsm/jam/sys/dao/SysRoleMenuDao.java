package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.CrudDao;
import com.bsm.jam.sys.entity.SysRoleMenu;
import org.springframework.stereotype.Component;

/**
 *
 * @author chen
 * @date 2017/10/26
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe:
 */
@Component
public interface SysRoleMenuDao  extends CrudDao<SysRoleMenu> {
}
