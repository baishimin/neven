package com.bsm.jam.sys.service.impl;

import com.bsm.jam.base.service.impl.BaseServiceImpl;
import com.bsm.jam.sys.dao.SysMenuDao;
import com.bsm.jam.sys.entity.SysMenu;
import com.bsm.jam.sys.service.ISysMenuService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *
 * 菜单服务实现
 * @author baijun
 * @date 2018-4-12 15:05:05
 */
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuDao,SysMenu> implements ISysMenuService {

    @Override
    public List<SysMenu> caseMenu(Long userId) {
        Map<Long,List<SysMenu>> map = new HashMap();
        //菜单列表
        List<SysMenu> menus = this.baseMapper.selectMenusByUserId(userId);

        for (SysMenu sysMenu: menus) {
            List<SysMenu> parentMenu = map.get(sysMenu.getParentId());
            if(null == parentMenu){
                parentMenu = new ArrayList<SysMenu>();
            }
            parentMenu.add(sysMenu);
            map.put(sysMenu.getParentId(),parentMenu);
        }
        List<SysMenu> resultList = makeMenu(map,0L);
        Collections.sort(resultList);
        return resultList;
    }


    public List<SysMenu> makeMenu(Map<Long, List<SysMenu>> map, Long supId) {
        List<SysMenu> sysMenus = new ArrayList();
        List<SysMenu> menuList = map.get(supId);
        if (menuList != null) {
            for (SysMenu me : menuList) {
                me.setChildren(makeMenu(map, me.getId()));
                sysMenus.add(me);
            }
        }
        return sysMenus;
    }
}
