package com.bsm.jam.sys.dao;


import com.bsm.jam.base.dao.TreeDao;
import com.bsm.jam.sys.entity.SysDict;

/**
 * Created by chen on 2017/4/10.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 系统数据字典dao
 */
public interface SysDictDao  extends TreeDao<SysDict> {
}
