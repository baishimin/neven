package com.bsm.jam.sys.service;

import com.bsm.jam.base.service.IBaseService;
import com.bsm.jam.sys.entity.SysMenu;

import java.util.List;

/**
 * 类的功能描述
 *    菜单管理接口
 * @author baijun
 * @data 2017年12月22日17:03:27
 */
public interface ISysMenuService extends IBaseService<SysMenu> {

    /**
     * 侧边菜单查询
     * @param userId 用户编号
     * @return
     */
    List<SysMenu> caseMenu(Long userId);
}
