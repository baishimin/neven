package com.bsm.jam.shiro;

import org.apache.shiro.SecurityUtils;

/**
 *
 * shiro 用户封装的工具类
 * @author baijun
 * @date 2018-4-16 14:53:27
 */
public class NevenShiroUser {

    /**
     * 取出Shiro中的当前用户LoginName.
     */
    public static String name() {
        return ShiroUser().getName();
    }

    public static Long id() {
        return ShiroUser().getId();
    }

    public static String loginName() {
        return ShiroUser().getUsername();
    }

    public static ShiroUser ShiroUser() {
        ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return user;
    }
}
