package com.bsm.jam.shiro;

import java.io.Serializable;

/**
 *
 * 自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息.
 * @author baijun
 * @date 2018-4-16 14:49:16
 */
public class ShiroUser implements Serializable{

    private static final long serialVersionUID = -1376529886724181319L;

    private Long id;

    private String username;

    private String name;

    public ShiroUser(Long id, String username, String name) {
        this.id = id;
        this.username = username;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
