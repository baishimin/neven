package com.bsm.jam.base.service;

import com.baomidou.mybatisplus.service.IService;
import com.bsm.jam.base.vo.DataTable;

/**
 * 类的描述
 *   service 公用方法
 * @author baijun
 * @data 2017年12月22日15:58:48
 */
public interface IBaseService<T> extends IService<T> {

    /**
     *  DataTable 分页查询
     * @param dt
     * @return
     */
    DataTable<T> queryPageSearch(DataTable<T> dt);
}
