package com.bsm.jam.base.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * DAO支持类实现
 *
 * @author baijun
 * @date 2018-4-12 14:52:04
 */


public interface BaseDao<T> extends BaseMapper<T> {

     void insert(String tableName) ;
}
