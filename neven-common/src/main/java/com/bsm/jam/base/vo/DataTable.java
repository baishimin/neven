package com.bsm.jam.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * DataTable 分页实体
 * @author baijun
 * @data 2017年12月22日16:02:19
 */
public class DataTable<T> implements Serializable {
    private static final long serialVersionUID = 2252240868205663450L;


    private int count;
    /**
     * 搜索条件
     */
    private Map<String, Object> searchParams;

    /**
     * 返回列表
     */
    private List<T> data = new ArrayList<T>();

    /**
     * 排序 条件
     */
    Map<String, String> sorts;

    /**
     * 当前页码
     */
    private int page;

    /**
     * 页码大小
     */
    private int limit;

    /**
     * 返回code
     */
    private String code;


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Map<String, Object> getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(Map<String, Object> searchParams) {
        this.searchParams = searchParams;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Map<String, String> getSorts() {
        return sorts;
    }

    public void setSorts(Map<String, String> sorts) {
        this.sorts = sorts;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
