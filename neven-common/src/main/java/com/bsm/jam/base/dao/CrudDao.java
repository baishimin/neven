package com.bsm.jam.base.dao;

/**
 * 类的功能描述
 *   crud dao 功能操作
 * @author baijun
 * @data 2017年12月21日16:33:03
 */
public interface CrudDao<T> extends BaseDao<T> {
}
