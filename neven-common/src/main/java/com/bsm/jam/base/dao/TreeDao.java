package com.bsm.jam.base.dao;


import com.bsm.jam.base.entity.TreeEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 树型结构的DAO支持类实现
 *
 * @author baijun
 * @date 2018-4-12 14:52:04
 */
@Component
public interface TreeDao<T extends TreeEntity<T>> extends CrudDao<T> {

    /**
     * 找到所有子节点
     * @param entity
     * @return
     */
     List<T> findByParentIdsLike(T entity);

    /**
     * 更新所有父节点字段
     * @param entity
     * @return
     */
     int updateParentIds(T entity);

}