package com.bsm.jam.constant;

/**
 *
 * 系统的参数设置 ,后面需要放到数据和缓存中
 * @author baijun
 * @date 2018-4-16 15:09:31
 */
public class Setting {

    public static final String HASH_ALGORITHM = "SHA-1";
    public static final int HASH_INTERATIONS = 1024;
    public static final int SALT_SIZE = 8;
}
