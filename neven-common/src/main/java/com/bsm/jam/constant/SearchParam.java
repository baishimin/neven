package com.bsm.jam.constant;


/**
 *
 * 搜索条件 常量
 * @author baijun
 * @date 2018-4-12 14:55:13
 */
public class SearchParam {
    /**
     * 等于
     */
    public static final String SEARCH_EQ="search_eq_";

    /**
     * 左模糊
     */
    public static final String SEARCH_LLIKE="search_llike_";

    /**
     * 右模糊
     */
    public static final String SEARCH_RLIKE="search_rlike_";

    /***
     * 全模糊
     */
    public static final String SEARCH_LIKE="search_like_";

}
