<!DOCTYPE html>
<html lang="ZH-cn">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="${base}/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="${base}/css/login.css">
    <link href="${base}/css/plugins/toastr/toastr.min.css" rel="stylesheet">
</head>

<body>
<div class="kit-login">
    <div class="kit-login-bg"></div>
    <div class="kit-login-wapper">
        <h2 class="kit-login-slogan">欢迎使用 <br> 北极星 1.0 后台模板</h2>
        <div class="kit-login-form">
            <h4 class="kit-login-title">登录</h4>
            <form class="layui-form" method="post" action="${base}/login" >
                <div class="kit-login-row">
                    <div class="kit-login-col">
                        <i class="layui-icon">&#xe612;</i>
                        <span class="kit-login-input">
                <input type="text" name="username" lay-verify="required" placeholder="用户名/邮箱/手机号" />
              </span>
                    </div>
                    <div class="kit-login-col"></div>
                </div>
                <div class="kit-login-row">
                    <div class="kit-login-col">
                        <i class="layui-icon">&#xe64c;</i>
                        <span class="kit-login-input">
                <input type="password" name="password" lay-verify="required" placeholder="密码" />
              </span>
                    </div>
                    <div class="kit-login-col"></div>
                </div>
                <div class="kit-login-row">
                    <div class="kit-login-col">
                        <input type="checkbox" name="rememberMe" title="记住帐号" lay-skin="primary">
                    </div>
                </div>
                <div class="kit-login-row">
                    <button class="layui-btn kit-login-btn" lay-submit="submit" lay-filter="login_hash">登录</button>
                </div>
                <div class="kit-login-row" style="margin-bottom:0;">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="${base}/js/jquery.min.js?v=2.1.4"></script>
<script src="${base}/plugins/polyfill.min.js"></script>
<script src="${base}/plugins/toastr/toastr.min.js"></script>
<script src="${base}/plugins/toastr/toast.js"></script>
<script src="${base}/plugins/layui/layui.js"></script>
<script>

    $(document).ready(function () {

    <#if error??>
        toast_error("",'${error}');
    </#if>

    });
    layui.use(['layer', 'form'], function() {
        var form = layui.form,
                $ = layui.jquery;

        $('#forgot').on('click', function() {
            layer.msg('请联系管理员.');
        });
        //监听提交
        form.on('submit(login_hash)', function(data) {
            setTimeout(function(){
                $(".layui-form").submit();
            },1000);
            return false;
        });
    });
</script>
</body>

</html>