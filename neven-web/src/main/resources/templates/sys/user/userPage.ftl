<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户信息</title>
    <link rel="stylesheet" href="${rc.contextPath}/plugins/layui/css/layui.css" media="all"/>
</head>
<style>
    .layui-form-query {
        padding: 10px 15px 0px 15px;
    }
</style>
<body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space1">
            <div class="layui-col-md12">
                <div class="layui-form-query" lay-filter="user">
                    <form class="layui-form" id="query_form">
                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-mid">id：</label>
                                <div class="layui-input-inline" style="width: 100px;">
                                    <input type="tel" name="search_eq_login_id" id="id" autocomplete="off" class="layui-input"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <label class="layui-form-mid">用户名：</label>
                                <div class="layui-input-inline" style="width: 100px;">
                                    <input type="text" name="search_eq_login_name" id="loginName" autocomplete="off" class="layui-input"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" type="button" id="query" data-type="reload"><i class="layui-icon">&#xe615;</i>查询</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="layui-col-md12">
                <div class="layui-row ">
                    <div class="layui-col-md12">
                        <button class="layui-btn" id="addUser">
                            <i class="layui-icon">&#xe654;</i>新增
                        </button>
                        <button class="layui-btn layui-btn-danger" function="submit" method="get" url="/fsbus/1002" isMutiDml="1" isConfirm="1" confirmMsg="是否确定删除选中的数据？" inputs="id:">
                            <i class="layui-icon">&#xe640;</i>批量删除
                        </button>
                        <button class="layui-btn" id="refresh">
                            <i class="layui-icon">&#x1002;</i>刷新
                        </button>
                    </div>
                    <div class="layui-col-md12">
                        <table class="layui-hide" id="LAY_table_user" lay-filter="user"></table>
                        <script type="text/html" id="barDemo">
                            <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
                            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
                            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addUserModel" style="display: none">
        <div class="site-text site-block">
            <form class="layui-form" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">输入框</label>
                    <div class="layui-input-block">
                        <input type="text" name="title" required="" lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">密码框</label>
                    <div class="layui-input-inline">
                        <input type="password" name="password" required="" lay-verify="required" lay-vertype="tips" placeholder="请输入密码" autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-form-mid layui-word-aux">辅助文字</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">选择框</label>
                    <div class="layui-input-block">
                        <select name="city" lay-verify="required">
                            <option value=""></option>
                            <option value="0">北京</option>
                            <option value="1">上海</option>
                            <option value="2">广州</option>
                            <option value="3">深圳</option>
                            <option value="4">杭州</option>
                        </select><div class="layui-unselect layui-form-select"><div class="layui-select-title"><input type="text" placeholder="请选择" value="" readonly="" class="layui-input layui-unselect"><i class="layui-edge"></i></div><dl class="layui-anim layui-anim-upbit"><dd lay-value="" class="layui-select-tips">请选择</dd><dd lay-value="0" class="">北京</dd><dd lay-value="1" class="">上海</dd><dd lay-value="2" class="">广州</dd><dd lay-value="3" class="">深圳</dd><dd lay-value="4" class="">杭州</dd></dl></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">复选框</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="like[write]" title="写作"><div class="layui-unselect layui-form-checkbox" lay-skin=""><span>写作</span><i class="layui-icon"></i></div>
                        <input type="checkbox" name="like[read]" title="阅读" checked=""><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin=""><span>阅读</span><i class="layui-icon"></i></div>
                        <input type="checkbox" name="like[dai]" title="发呆"><div class="layui-unselect layui-form-checkbox" lay-skin=""><span>发呆</span><i class="layui-icon"></i></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">开关</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="switch" lay-skin="switch" lay-text="ON|OFF" lay-filter="switchTest" value="1"><div class="layui-unselect layui-form-switch" lay-skin="_switch"><em>OFF</em><i></i></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">单选框</label>
                    <div class="layui-input-block">
                        <input type="radio" name="sex" value="男" title="男"><div class="layui-unselect layui-form-radio"><i class="layui-anim layui-icon"></i><div>男</div></div>
                        <input type="radio" name="sex" value="女" title="女" checked=""><div class="layui-unselect layui-form-radio layui-form-radioed"><i class="layui-anim layui-icon"></i><div>女</div></div>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">文本域</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit="" lay-filter="formDemo">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</body>

<script src="${rc.contextPath}/plugins/layui/layui.js"></script>
<script src="${rc.contextPath}/js/jquery.min.js"></script>

<script>
    var table = "";
    layui.use('table', function(){
        table = layui.table;

        //方法级渲染
        table.render({
            elem: '#LAY_table_user'
            ,url: '/sys/userPage'
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'id', title: 'ID', sort: true, fixed: true}
                ,{field:'loginName', title: '用户名'}
                ,{field:'name', title: '姓名', sort: true}
                ,{field:'email', title: '邮件'}
                ,{field:'mobile', title: '电话'}
                ,{field:'phone', title: '移动手机', sort: true}
                ,{fixed: 'right',title:'操作', align:'center', toolbar: '#barDemo'}
            ]]
            ,id: 'testReload'
            ,page: true
            ,height:'full-135'

        });


        //监听表格复选框选择
        table.on('checkbox(user)', function(obj){
            console.log(obj)
        });
        //监听工具条
        table.on('tool(user)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                layer.msg('ID：'+ data.id + ' 的查看操作');
            } else if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                layer.alert('编辑行：<br>'+ JSON.stringify(data))
            }
        });

        var $ = layui.$, active = {
            reload: function(){
                //执行重载
                table.reload('testReload', {
                    where: {
                        searchParams: {
                            search_eq_login_name: $('#loginName').val(),
                            search_eq_id:$("#id").val()
                        }
                    }
                });
            }
        };

        $('#query').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });


    $('#addUser').on('click', function(){
        var $addUserModel = $("#addUserModel");
        layer.open({
            type: 1,
            area: ['600px', 'auto'],
            shadeClose: true, //点击遮罩关闭
            content: $addUserModel,
            btn: ['提交','关闭'],
            yes: function(index, layero){
                //按钮【按钮一】的回调
                layer.msg("确认");
            },
            cancel: function(){

                return false;// 开启该代码可禁止点击该按钮关闭
            }
        });
    });

    //刷新
    $('#refresh').on('click', function(){
        var ii = layer.load();
        //此处用setTimeout演示ajax的回调
        setTimeout(function(){
            table.reload();
            layer.close(ii);
        }, 1000);
    });

</script>
</html>