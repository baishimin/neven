package com.bsm.jam.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;


/**
 *
 * 如果我们需要在Spring容器完成Bean的实例化，配置和其他的初始化后添加一些自己的逻辑处理，
 * 我们就可以定义一个或者多个BeanPostProcessor接口的实现。
 * @author baijun
 * @date 2018年4月10日17:44:18
 */
@Configuration
public class InstallBean implements BeanPostProcessor {
    /** 日志 */
    private final Logger logger = LoggerFactory.getLogger(InstallBean.class);
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        logger.info("哟吼,对象{}开始实例化",beanName);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        logger.info("哟吼,对象{}实例化完成",beanName);
        return bean;
    }
}
