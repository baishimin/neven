package com.bsm.jam.config.sitemesh;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 *
 * SiteMesh装饰器
 * @author  baijun
 * @date 2018-4-12 15:06:58
 */
public class WebSiteMeshFilter extends ConfigurableSiteMeshFilter{

    /** 需要装饰的访问路径 */
    private String contextPath = "/sys,/index.htm";

    /** 装饰器页面路径*/
    private String decoratorPath = "/layouts";

    /** 不需要装饰的访问路径,多个之间用英文逗号分隔 */

    private String excludedPath ="*.js,*.css";


    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        //通过配置文件
        if(null == contextPath){
            return;
        }
        String[] contextPaths = contextPath.split(",");
        for (String path: contextPaths) {
            builder.addDecoratorPath(path,decoratorPath);
        }

        if(null == decoratorPath){
            return;
        }

        String[] excludedPaths = excludedPath.split(",");
        for (String path: excludedPaths) {
            builder.addExcludedPath(path);
        }

        builder.addTagRuleBundle(new MyTagRuleBundle());
    }
}
