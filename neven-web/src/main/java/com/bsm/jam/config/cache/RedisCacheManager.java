package com.bsm.jam.config.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 *
 * Redis缓存管理
 * @author baijun
 * @date 2018-4-13 14:29:03
 */
public class RedisCacheManager implements CacheManager{

    /** 日志 */
    private static final Logger logger = LoggerFactory.getLogger(RedisCacheManager.class);

    private RedisTemplate redisTemplate;

    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return new RedisCache<K, V>(s,redisTemplate);
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}
