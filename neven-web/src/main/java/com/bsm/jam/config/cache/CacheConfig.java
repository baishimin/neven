package com.bsm.jam.config.cache;

import org.crazycake.shiro.RedisManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.crazycake.shiro.RedisCacheManager;

/**
 *
 * 缓存配置中心
 * @author baijun
 * @date 2018-4-13 14:29:03
 */
@Configuration
@EnableCaching
public class CacheConfig {
    /** 日志 */
    private Logger logger = LoggerFactory.getLogger(CacheConfig.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Bean(name = "redisCacheManager")
    public RedisCacheManager redisCacheManager(){
        logger.info("-------redis cache init-------");
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        redisCacheManager.setRedisManager(redisManager());
        logger.info("-------redis cache -------:{}",redisCacheManager);
        return redisCacheManager;
    }

    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.password}")
    private String password;
    @Value("${spring.redis.port}")
    private int port;
    @Value("${spring.redis.database}")
    private int database;

    public RedisManager redisManager() {
        RedisManager redisManager = new RedisManager();
        redisManager.setHost(host);
        redisManager.setPassword(password);
        redisManager.setPort(port);
        redisManager.setDatabase(database);
        return redisManager;
    }

}
