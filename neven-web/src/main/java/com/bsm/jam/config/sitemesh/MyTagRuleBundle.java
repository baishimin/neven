package com.bsm.jam.config.sitemesh;

import org.sitemesh.SiteMeshContext;
import org.sitemesh.content.ContentProperty;
import org.sitemesh.content.tagrules.TagRuleBundle;
import org.sitemesh.content.tagrules.html.ExportTagToContentRule;
import org.sitemesh.tagprocessor.State;

/**
 *
 * 自定义 Tag
 * @author  baijun
 * @date 2018-4-12 15:06:58
 */
public class MyTagRuleBundle implements TagRuleBundle{

    @Override
    public void install(State state, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {
        state.addRule("bsm_js",
                new ExportTagToContentRule(siteMeshContext,contentProperty.getChild("bsm_js"), false));
    }

    @Override
    public void cleanUp(State state, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {

    }
}
