package com.bsm.jam.controller;

import com.alibaba.fastjson.JSON;
import com.bsm.jam.base.common.Constant;
import com.bsm.jam.base.vo.DataTable;
import com.bsm.jam.sys.entity.SysUser;
import com.bsm.jam.sys.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 用户管理
 * @author baijun
 * @date 2018-4-20 16:41:21
 */
@Controller
@RequestMapping("/sys")
public class SysUserController {

    private final Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private ISysUserService sysUserService;

    @GetMapping("/user")
    public String userIndex(){
        return "sys/user/userPage";
    }


    @RequestMapping(value = "/userPage")
    @ResponseBody
    public DataTable<SysUser> userPageList(DataTable<SysUser> dt){
        logger.info("用户列表查询 -- > dt:{}", JSON.toJSONString(dt));
        DataTable<SysUser> userDateTable = null;
        try {
            userDateTable =  sysUserService.queryPageSearch(dt);
            userDateTable.setCode(Constant.RESULT.CODE_YES.getValue());
        } catch (Exception e) {
            logger.info("用户列表查询 -- > 异常:{}", e.getMessage());
            userDateTable.setCode(Constant.RESULT.CODE_NO.getValue());
            e.printStackTrace();
        }
        return userDateTable;
    }

}
