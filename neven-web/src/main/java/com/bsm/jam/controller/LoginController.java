package com.bsm.jam.controller;

import com.bsm.jam.sys.service.ISysMenuService;
import com.bsm.jam.utils.Result;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 类的功能描述:
 *      登录控制
 * @author baijun
 * @date 2017年12月19日13:57:01
 */
@Controller
public class LoginController {

    /**
     * 菜单服务
     */
    @Autowired
    private ISysMenuService sysMenuService;


    /**
     *  空地址请求
     * @param model
     * @param request
     * @return
     */
    @GetMapping(value = "")
    public String index(Model model, HttpServletRequest request) {
        model.addAttribute("base", request.getContextPath());
        Subject s = SecurityUtils.getSubject();
        return s.isRemembered() || s.isAuthenticated() ? "redirect:index.htm" : "sign/login";
    }

    /**
     * index 界面
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    public String index(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        return "home/index";
    }

    @RequestMapping(value = "/main.htm", method = RequestMethod.GET)
    public String main(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        return "home/index";
    }

    /**
     * 登录 界面
//     * @param model
//     * @param request
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(Model model, HttpServletRequest request) {
        model.addAttribute("base", request.getContextPath());
        Subject s = SecurityUtils.getSubject();

        return s.isRemembered() || s.isAuthenticated() ? "redirect:index.htm" : "sign/login";
    }


    /**
     * 登录出错界面--实际的登录 交给了 shiro
     * @param request
     * @param username
     * @param model
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String fail(HttpServletRequest request,String username, String password,Model model) {
        model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);

        String error=(String)request.getAttribute(org.apache.shiro.web.filter.authc.FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
        if(error!=null){
            if(error.contains("DisabledAccountException")){
                model.addAttribute("error","用户已被屏蔽,请登录其他用户.");
            }else if(error.contains("UnknownAccountException")){
                model.addAttribute("error","用户不存在,请检查后重试!");
            }else if(error.contains("IncorrectCredentialsException")){
                model.addAttribute("error","密码错误,请检查密码!");
            }else{
                model.addAttribute("error","登录失败，请重试.");
            }
        }
        return "sign/login";
    }

}
