package com.bsm.jam.controller;

import com.bsm.jam.shiro.NevenShiroUser;
import com.bsm.jam.shiro.ShiroUser;
import com.bsm.jam.sys.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 装饰器Controller
 * @author baijun
 * @date 2018年4月10日15:25:34
 */
@Controller
public class SiteMeshController {

    @Autowired
    private ISysMenuService menuService;

    @GetMapping(value = "layouts")
    public String getIndex(Model model,HttpServletRequest request){
        model.addAttribute("neven",NevenShiroUser.ShiroUser());
        model.addAttribute("menus",menuService.caseMenu(NevenShiroUser.id()));
        return "layouts/default";
    }


    @PostMapping(value = "layouts")
    public String postIndex(Model model,HttpServletRequest request){
        return getIndex(model,request);
    }
}
