package com.bsm.jam.sys;

import com.alibaba.fastjson.JSON;
import com.bsm.jam.BaseTests;
import com.bsm.jam.base.vo.DataTable;
import com.bsm.jam.sys.entity.SysMenu;
import com.bsm.jam.sys.entity.SysUser;
import com.bsm.jam.sys.service.ISysMenuService;
import com.bsm.jam.sys.service.ISysUserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * SYS 测试集合类
 * Created by bai on 2018/4/20.
 */
public class SysTest extends BaseTests{

    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private ISysUserService userService;
    @Test
    public void menuTest(){
        List<SysMenu> sysMenus = sysMenuService.caseMenu(1L);
        for (SysMenu sys : sysMenus) {
            System.out.println(JSON.toJSONString(sys));
        }
    }

    @Test
    public void userTest(){

        DataTable dt = new DataTable();
        dt.setPage(1);
        dt.setLimit(10);
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("search_eq_login_name","");
        dt.setCount(0);
        Map<String,String> mapSort = new HashMap<String, String>();
        mapSort.put("createDate","asc");
        dt.setSorts(mapSort);
        dt.setSearchParams(map);
        DataTable<SysUser> sysUser = userService.queryPageSearch(dt);
        for (SysUser user: sysUser.getData()) {
            System.out.println(JSON.toJSONString(user));
        }

    }



    @Test
    public void userAllTest(){

        SysUser sysUser = userService.selectById(1);
        System.out.println(sysUser.getId());


    }
}
