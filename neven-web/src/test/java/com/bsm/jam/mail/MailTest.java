package com.bsm.jam.mail;

import com.bsm.jam.BaseTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 *
 * Email 测试
 * @author baijun
 * @date 2018-4-16 11:35:25
 */
public class MailTest extends BaseTests{

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void sendSimpleMail() throws Exception {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("15922406@qq.com");
        message.setTo("15922406@qq.com");
        message.setSubject("主题：简单邮件");
        message.setText("测试邮件内容");

    }
}
