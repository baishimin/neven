//package com.bsm.jam.redis;
//
//import com.bsm.jam.BaseTests;
//import org.junit.Test;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.ValueOperations;
//
//import javax.annotation.Resource;
//
///**
// *
// * redis 测试
// * @author baijun
// * @date 2018-4-13 16:46:22
// */
//public class RedisTest extends BaseTests{
//
//
//    @Resource
//    private RedisTemplate<String,Object> redisTemplate;
//
//    @Test
//    public void redisSet(){
//        System.out.println(redisTemplate);
//        ValueOperations<String,Object> vo = redisTemplate.opsForValue();
//        vo.set("username", "柏世民");
//    }
//
//    @Test
//    public void redisSGet(){
//        System.out.println(redisTemplate);
//        ValueOperations<String,Object> vo = redisTemplate.opsForValue();
//        String username = (String) vo.get("username");
//        System.out.println(username);
//    }
//
//}
